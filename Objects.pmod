class Proto(mapping(string:mixed) _data) {
    mixed `->(string attr) {
        return ::`->(attr, this) || _data[?attr];
    }

    mixed `[](string attr) {
        return ::`[](attr, this) || _data[?attr];
    }

    void update(Proto|mapping(string:mixed) src) {
        if (mappingp(src)) {
            _data |= src;
        }
        else {
            _data |= src->_data;
        }
    }

    object `->=(string attr, mixed value) {
        _data[attr] = value;
        return this;
    }

    object `[]=(string attr, mixed value) {
        _data[attr] = value;
        return this;
    }
}

class Channel {
    inherit Proto;
}

class Event {
    inherit Proto;

    bool ok() {
        return !!_data->ok;
    }

    string body() {
        if (stringp(this?->text))
            return this->text;
        return "";
    }
}

class Group {
    inherit Proto;
}

class Team {
    inherit Proto;
}

class User {
    inherit Proto;
}

class Encodable {
    mapping(string:mixed) encode_mapping() {
        return ([ ]);
    };

    final string encode_json() {
        return Standards.JSON.encode(this->encode_mapping());
    }
}
