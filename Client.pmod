import ".";
import Objects;
import Messaging;
import Connection;

class Client {
    inherit Proto;

    protected mapping(string:Channel) channels = ([ ]);
    protected mapping(string:Group) groups = ([ ]);
    protected mapping(string:User) users = ([ ]);
    protected Team team;
    protected array bots;
    private Connection con;

    void create(void|string token, void|Connection _conn) {
        if (token && stringp(token)) {
            con = Connection(token, event_handler);
        }
        else if (_conn) {
            con = _conn;
            con->set_event_handler(event_handler);
        }
        else {
            error("Neither token nor Connection were given!\n");
        }
    }

    int connect() {
        Event status = con->connect();
        if (objectp(status))
            event_handler(status);

        return -1;
    }

    void event_handler(Event event) {
        if (event->ok() && !event?->type && event?->reply_to) {
            ;
        }
        else if (event?->type && functionp(this[event->type])) {
            call_out(this[event->type], 0, event);
        }
        else {
            if (event?->error) {
                werror("Received error in replay to %d: errno %d: %s\n",
                        event->reply_to, event->error->code, event->error->msg);
            }
            else if (!event || !event->type) {
                werror("There were disturbance in the Force and something went "
                        "wrong with the connection or received data: %O\n", event);
            }
            else if (!functionp(this[event->type])) {
                werror("Unhandled response type: %O%s\n", event?->type,
                        event?->subtype ? " subtype: " + event->subtype : "");
            }
        }
    }

    protected int send(Encodable msg) {
        return con->send(msg);
    }

    protected void add_channel(Channel channel) {
        if (!channels[channel->id])
            channels[channel->id] = channel;
        else
            channels[channel->id]->update(channel);
    }

    protected void add_group(Group group) {
        if (!groups[group->id])
            groups[group->id] = group;
        else
            groups[group->id]->update(group);
    }

    protected void add_user(User user) {
        if (!users[user->id])
            users[user->id] = user;
        else
            users[user->id]->update(user);
    }

    void channel_created(Event event) {
        add_channel(Channel(event->_data));
    }

    void channel_joined(Event event) {
        add_channel(Channel(event->_data));
    }

    void group_created(Event event) {
        add_group(Group(event->_data));
    }

    void group_joined(Event event) {
        add_group(Group(event->_data));
    }

    void desktop_notification(Event event) {
    }

    void hello(Event event) {
        con->ping();
    }

    void presence_change(Event event) {
        User user = User(
            ([
                "id": event->user,
                "presence": event->presence
            ])
        );
        add_user(user);
    }

    void rtm_start(Event event) {
        _data = event->self;
        team = event->team;

        foreach (event->users, mapping(string:mixed) user)
            users[user->id] = User(user);

        foreach (event->channels, mapping(string:mixed) channel)
            channels[channel->id] = Channel(channel);

        foreach (event->groups, mapping(string:mixed) group)
            groups[group->id] = Group(group);

        bots = event->bots;
    }

    void team_join(Event event) {
        add_user(User(event->user));
    }

    void user_change(Event event) {
        User user = User(event->user);
        add_user(user);
    }

    void user_typing(Event event) {
    }
}
