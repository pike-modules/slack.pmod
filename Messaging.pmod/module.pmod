import "..";
import Objects;

class Message(void|string type) {
    inherit Encodable;

    private int id;

    final void set_id(int i) {
        id = i;
    }

    final int get_id() {
        return id;
    }

    mapping(string:mixed) encode_mapping() {
        return ([
            "id": get_id(),
            "type": type,
        ]);
    };
}

class Ping {
    inherit Message;

    string type = "ping";
}
