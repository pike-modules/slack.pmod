import ".";
import "..";
import Objects;

class Block {
    inherit Encodable;

    protected string type = "section";

    mapping(string:mixed) encode_mapping() {
        return ([ "type": type ]);
    }
}

class Accessory {
    inherit Encodable;

    protected string type;

    mapping(string:mixed) encode_mapping() {
        if (stringp(type) && strlen(type))
            return ([ "type": type ]);
        else
            return UNDEFINED;
    }
}

class TextBlock(string text, void|Accessory accessory) {
    inherit Block;

    mapping(string:mixed) encode_mapping() {
        mapping(string:mixed) data = ([
            "type": type,
            "text": ([
                "type": "mrkdwn",
                "text": text,
            ])
        ]);

        if (objectp(Accessory))
            data->accessory = accessory;

        return data;
    }
}

class ImageBlock(string image_url, string alt_text) {
    inherit Block;

    protected string type = "image";

    mapping(string:mixed) encode_mapping() {
        return ([
            "type": type,
            "image_url": image_url,
            "alt_text": alt_text,
        ]);
    }
}

class DividerBlock {
    inherit Block;

    protected string type = "divider";
}

class PostMessage {
    inherit Message;

    protected string type = "api";
    string api = "chat.postMessage";
    private string channel;
    private bool as_user = true;
    private string parse = "full";
    private array(Block) blocks = ({ });
    private string username;

    void create(string _channel, void|string user) {
        channel = _channel;
        if (stringp(user)) {
            username = user;
            as_user = false;
        }
    }

    void set_username(string name) {
        as_user = false;
        username = name;
    }

    void add(Block block) {
        blocks += ({ block });
    }

    mapping(string:mixed) encode_mapping() {
        mapping(string:mixed) data = ([
            "as_user": as_user,
            "blocks": blocks,
            "channel": channel,
            "parse": parse,
            "text": "",
            "username": username,
        ]);

        return data;
    }
}

class PostEphemeral {
    inherit PostMessage;

    string api = "chat.postEphemeral";
}

class ImageAccessory(string img_url, string alt_text) {
    inherit Accessory;

    protected string type = "image";

    mapping(string:mixed) encode_mapping() {
        return ([
            "type": type,
            "image_url": img_url,
            "alt_text": alt_text,
        ]);
    }
}

class ButtonAccesory {
    inherit Accessory;

    protected string type = "button";
}

PostMessage post_reply(Event event, void|string text) {
    PostMessage msg = PostMessage(event->channel);
    if (stringp(text))
        msg->add(TextBlock(text));
    return msg;
}

PostEphemeral ephemeral_reply(Event event, void|string text) {
    PostEphemeral msg = PostEphemeral(event->channel);
    if (stringp(text))
        msg->add(TextBlock(text));
    return msg;
}
