import ".";
import Objects;
import Messaging;

class Connection {
    private int last_ping = 0;
    private Standards.URI url;
    private int id = 1;
    private function event_handler = lambda(Event event) { return false; };
    private string token;
    private Protocols.WebSocket.Connection con;

    void create(string _token, void|function ev_handler) {
        token = _token;
        if (!undefinedp(ev_handler)) {
            event_handler = ev_handler;
        }
    }

    Event connect() {
        if (!Api.test()) {
            error("Api seems to be not working. api.test call failed "
                    " or returned not ok.\n");
            return UNDEFINED;
        }

        Event status = Api.rtm_start(token);

        if (!status || !status->ok()) {
            werror("Failed to start RTM. Reason: %O\n", status?->error);
            return UNDEFINED;
        }

        url = Standards.URI(status->url);
        con = Protocols.WebSocket.Connection();
        con->onmessage = got_data;
        con->connect(url);

        id = 1;

        return status;
    }

    private void got_data(Protocols.WebSocket.Frame f, mixed data) {
        if (data && f->opcode == Protocols.WebSocket.FRAME_TEXT) {
            Event event = Api.parse_response(f->text);

            if (event->type && this[event->type] && functionp(this[event->type])) {
                this[event->type](event);
            }
            else {
                event_handler(event);
            }
        }
    }

    void ping() {
        last_ping = send(Ping());
        call_out(ping_timeout, 9);
    }

    private void ping_timeout() {
        remove_call_out(ping_timeout);
        remove_call_out(ping);
        reconnect();
    }

    void pong(Event event) {
        remove_call_out(ping_timeout);
        remove_call_out(ping);
        call_out(ping, 5);
        if (event->reply_to != last_ping)
            werror("Received pong to older ping or different message.\n");
    }


    void reconnect() {
        if (con) {
            mixed e = catch {
                con->close();
            };
            if (e) {
                werror("There was a problem closing connection socket.\n");
            }
            con = 0;
        }
        connect();
    }

    void reconnect_url(Event event) {
        url = Standards.URI(event->url);
    }

    void set_event_handler(function handler) {
        event_handler = handler;
    }

    int send(Encodable msg) {
        msg->set_id(id++);

        mixed err = catch {
            con->send_text(Standards.JSON.encode(msg));
        };

        if (err) {
            werror("Failed to send message: con: %O, con.state: %O, con.stream: %O, "
                    " con.stream.is_open(): %O\n",
                    con, con.state, con.stream, con.stream && con.stream->is_open());
            remove_call_out(ping_timeout);
            remove_call_out(ping);
            connect();
            call_out(send, 3, msg);
        }

        return msg->get_id();
    }
}
