import ".";
import Objects;

private string API_URL = "https://slack.com/api/";

Event parse_response(string data) {
    Event event;

    mixed e = catch {
        event = Event(Standards.JSON.decode_utf8(data));
    };

    if (e) {
        e = catch {
            event = Event(Standards.JSON.decode(data));
        };

        if (e) {
            werror("Error while parsing response: %O\n", describe_backtrace(e));
            return UNDEFINED;
        }
    }

    return event;
}

Event chat_postMessage(string token, Messaging.Chat.PostMessage msg) {
    return call(msg->api, token, msg);
}

Event call(string api_call, void|string token, void|mapping|Encodable data,
           void|mapping headers, void|mapping params) {
    if (!headers)
        headers = ([ ]);

    headers["Content-Type"] = "application/json";

    if (token)
        headers["Authorization"] = sprintf("Bearer %s", token);

    Protocols.HTTP.Query q;
    mixed e = catch {
        q = Protocols.HTTP.do_method("POST", API_URL + api_call, params, headers,
            UNDEFINED, Standards.JSON.encode(data));
    };

    if (e || !q || q.status != 200) {
        return UNDEFINED;
    }

    return parse_response(q.data());
}

bool test() {
    Event event = call("api.test");

    if (!event)
        return false;

    return !!event->ok;
}

Event rtm_start(string token, void|bool simple_latest, void|bool no_unreads,
                void|bool mpim_aware) {
    mapping params = ([
        "simple_latest": !!simple_latest,
        "no_unreads": !!no_unreads,
        "mpim_aware": !!mpim_aware
    ]);
    Event event = call("rtm.start", token, params);

    if (!event)
        return UNDEFINED;

    event->type = "rtm_start";
    return event;
}
